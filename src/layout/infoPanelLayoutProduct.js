import SpinnerLoader from '../components/spinnerComponent'
import { useEffect, useState } from 'react'
import axios from 'axios'
import useAutentication from '../Autenticacion/useAutentication'
import DataList from '../components/DataList'
import SearchBarComponent from '../components/SearchBarComponent'
import useDebounce from '../components/useDebounce'
import useGetAll from '../components/useGetAll'

function InfoPanel(){
    const [ProductData, setProductData] = useState([])
    const [loaded, setLoadedState] = useState(false)
    const [ProductWanted, setProductWanted]= useState("")
    const [BusquedaLoader, setBusquedaLoader]= useState(true)
    let token= useAutentication()
    let AllProducts=useGetAll(token)
    const debouncedProductWanted=useDebounce(ProductWanted,100)
    
    
    const config={ headers: {'access-token': token}}
    useEffect(() => {
        const fetchData = async () => {
            if (!loaded && token) {
                const result = await axios.get('http://api.telematica.xkivertech.cl:4000/products',config)
                if (result.data) { 
                    setLoadedState(true)
                    setProductData(result.data)
                    setBusquedaLoader(false)
                }
            }
        }
        fetchData()
    })
    useEffect(()=>{
        if(debouncedProductWanted){
            console.log(ProductWanted)
            setBusquedaLoader(true)
            const config={ headers: {'access-token': token}}
            const fetchData = async () => {
                if (token && ProductWanted.length>=3) {
                    const result = await axios.get('http://api.telematica.xkivertech.cl:4000/products/'+ProductWanted,config)
                    if (result.data) { 
                        setProductData(result.data)
                        setBusquedaLoader(false)
                    }
                }
                else if(ProductWanted.length<3 || ProductWanted.length>=0 ){
                    setProductData(AllProducts)
                    setBusquedaLoader(false)
                }
                
            }
            fetchData()
        }
    },[debouncedProductWanted])
   
    
    if(BusquedaLoader){
        return(
            <SpinnerLoader dataLoaded={BusquedaLoader}/>
        )
    }
    else{
        return(
            <div>
                <SearchBarComponent ProductWanted={ProductWanted} setProductWanted={setProductWanted} />
                <DataList products={ProductData}/>
            </div>
            )
    }
}

export default InfoPanel