import { Spinner } from 'react-bootstrap'


function SpinnerLoader (props) {
    if (!props.dataLoaded) {
        return null
    } else {
        return (
            <div style={{position:"fixed", top:"50%", left:"50%", transform:"translate(-50%, -50%)"}}>
                <Spinner animation="grow" variant="dark" />
            </div>
        )
    }
}

export default SpinnerLoader