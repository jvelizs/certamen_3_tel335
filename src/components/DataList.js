import {Row, Col, Container,Card} from 'react-bootstrap'
function DataList(props) {
    return (
        <Container fluid className='bg-dark'>
            <Container className='bg-light' key={props.products}>
                <Row> 
                {
                props.products.map((product)=>
                    <Col xs={4} key={product.id}>
                        <Card className='mx-2 my-2' style={{height:175}}>
                            <Row className='justify-content-md-center'>
                                <Col>
                                    <img src={'https://'+product.image} width='60'></img>
                                    <h4>Nombre producto:{product.brand}</h4>
                                    <h4>Precio:${product.price}</h4>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                    )
                
                }
                </Row>
            </Container>
        </Container>
    
    
    
    
    
    
    
    
    
    
    
    
    )
}
export default DataList