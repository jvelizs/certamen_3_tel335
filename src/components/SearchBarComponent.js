import {Form} from 'react-bootstrap'



function SearchBarComponent(props){
    
    return(
        <div>
        <Form.Group>
            <Form.Control placeholder="Search Product" value={props.ProductWanted} onChange={(value)=>{
                props.setProductWanted(value.target.value)
                }} enabled="true" />
        </Form.Group>
        </div>
    )
}
export default SearchBarComponent