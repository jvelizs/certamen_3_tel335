import axios from 'axios'
import {  useState } from 'react'
const useGetAll=(token)=>{
    const [Producto, setProducto] = useState([])
    const config={ headers: {'access-token': token}}
    const fetchData = async () => {
        if (token) {
            const result = await axios.get('http://api.telematica.xkivertech.cl:4000/products',config)
            if (result.data) { 
                setProducto(result.data)
            }
        }
    }
    fetchData()
    return Producto
}
export default useGetAll