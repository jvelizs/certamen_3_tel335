import InfoPanel from './layout/infoPanelLayoutProduct'
import './App.css';

function App() {
  return (
    <div className="App">
        <InfoPanel/>
    </div>
  );
}

export default App;
